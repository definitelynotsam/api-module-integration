#!/bin/bash
# ########################################
# THIS IS A WORK IN PROGRESS
# ########################################


# ###############################################################################################
# This script will create a new issue based on a template tag.
#  On future iterations I want to run this on a weekly basis(maybe on a scheduled pipeline)
#  to create a work-log/journal type of issue weekly. The template will also change with time.
# ###############################################################################################
#
# To run the script pass the PAT and Project ID inline 
#
# ###############################################################################################

TOKEN="$1"
PROJECT_ID="$2"

echo "$DATE"

# getting issues with the label template
function get_template() {
  curl -s --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/issues?labels=template" | jq '.' > template_info.txt
}

# formatting the title and descriptions and posting into the API
# // TODO // 
# de-couple this function, is doing too much.
function format_template_info() {
  TITLE="'$DATE $(cat template_info.txt | grep "title" | cut -d ':' -f 2 | cut -d'"' -f 2)'"
  DESCRIPTION=$(cat template_info.txt | grep "description" | cut -d ':' -f 2 | sed 's/,$//') 

  curl --request POST --header "PRIVATE-TOKEN: $TOKEN" --header "Content-Type: application/json" --data '{"title":"'"${TITLE}"'", "description":'"${DESCRIPTION}"'}' "https://gitlab.com/api/v4/projects/$PROJECT_ID/issues"

}

# running the functions and deleting the template_info file
get_template
format_template_info
rm template_info.txt